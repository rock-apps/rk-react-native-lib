import PropTypes from 'prop-types'
import React from "react";
import {Text} from "react-native";
import {FONT, COLOR} from "../../../src/rkConfig/commonVariables";
import {getColor} from "./utils/commonStyle";

const TextRK = props => {

    const getFont = () => {
        let fontFamily = 'default'

        if (props.fontFamily === 'primary') {
            if (props.bold) {
                return {fontFamily: FONT.font_primary_bold}
            }
            return {fontFamily: FONT.font_primary}
        }

        if (props.bold) {
            return {fontFamily: FONT.font_default_bold}
        }

        return {fontFamily: FONT.font_default}
    }



    let textStyle = [
        props.style,
        {color: getColor(props.color)},
        getFont(),
        {fontSize: props.size || 14},
        props.uppercase && {textTransform: 'uppercase'}
    ]

    return <Text numberOfLines={props.numberOfLines} style={textStyle}>{props.text || props.children}</Text>
}

export default TextRK;

TextRK.propTypes = {
    bold: PropTypes.any,
    children: PropTypes.any,
    color: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    fontFamily: PropTypes.oneOf(['default', 'primary']),
    numberOfLines: PropTypes.number,
    size: PropTypes.number,
    style: PropTypes.object,
    text: PropTypes.string,
    uppercase: PropTypes.bool
}
