import PropTypes from 'prop-types'
import React from "react"
import {TouchableOpacity, Image} from "react-native"

const ImageIcon = props => {

    const {onPress, source, width, height, style} = props;


    if (onPress) {
        return (
            <TouchableOpacity
                onPress={() => onPress()}
                style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
            >

                <Image style={[{width: width, height: height}, style]} source={source}/>
            </TouchableOpacity>
        )
    }

    return <Image style={[{width: width, height: height}, style]} source={source}/>

}

export default ImageIcon

ImageIcon.propTypes = {
  height: PropTypes.number,
  onPress: PropTypes.func,
  source: PropTypes.any,
  width: PropTypes.number
}
