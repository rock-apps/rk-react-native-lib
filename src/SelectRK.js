import React from "react"
import {FONT, COLOR, DIMENSION} from "../../../src/rkConfig/commonVariables";
import RNPickerSelect from "react-native-picker-select";
import {getColor} from "./utils/commonStyle";

const SelectRK = props => {

    const {value, onValueChange, placeholder, items, style, fontColor, textAlign} = props

    let styles = {
        inputAndroid: {
            color: getColor(fontColor),
            backgroundColor: 'transparent',
            textAlign: textAlign || 'left',
            height: DIMENSION.inputHeight,
        },
        inputIOS: {
            height: DIMENSION.inputHeight,
            color: getColor(fontColor),
            textAlign: textAlign || 'left',
        },
        inputAndroidContainer: {paddingHorizontal: 15},
        inputIOSContainer: {paddingHorizontal: 15}
    }

    if (style && style.inputAndroid) {
        styles.inputAndroid = {
            ...styles.inputAndroid,
            ...style.inputAndroid
        }
    }
    if (style && style.inputIOS) {
        styles.inputIOS = {
            ...styles.inputIOS,
            ...style.inputIOS
        }
    }
    if (style && style.inputIOSContainer) {
        styles.inputIOSContainer = {
            ...styles.inputIOSContainer,
            ...style.inputIOSContainer
        }
    }

    if (style && style.inputAndroidContainer) {
        styles.inputAndroidContainer = {
            ...styles.inputAndroidContainer,
            ...style.inputAndroidContainer
        }
    }

    return (
        <RNPickerSelect
            style={styles}
            textInputProps={{height: DIMENSION.inputHeight}}
            pickerProps={{style: {fontFamily: FONT.font_input, marginTop: -50, color: 'transparent'}}}
            value={value}
            placeholderTextColor={getColor(fontColor)}
            placeholder={placeholder}
            onValueChange={onValueChange}

            items={items}
            useNativeAndroidPickerStyle={false}
        />
    )
}

export default SelectRK
