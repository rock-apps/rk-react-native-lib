import React, {useState} from "react";
import SelectRK from "./SelectRK";
import ColNB from "./ColNB";

const SelectLevelEducation = props => {

    const {input, inverted, placeholder, style, textAlign, fontColor} = props;
    const [valueInput, setValueInput] = useState(input.value)


    const options = [
        {value: "EFI", label: "Ensino Fundamental Incompleto"},
        {value: "EFC", label: "Ensino Fundamental Completo"},
        {value: "EMI", label: "Ensino Médio Incompleto"},
        {value: "EMC", label: "Ensino Médio Completo"},
        {value: "ESI", label: "Ensino Superior Incompleto"},
        {value: "ESC", label: "Ensino Superior Completo"},
        {value: "PGI", label: "Pós-Graduação Incompleto"},
        {value: "PGC", label: "Pós-Graduação Completo"},
    ];
    return (
        <ColNB style={{backgroundColor: 'transparent', flex: 0}}>
            <SelectRK
                textAlign={textAlign}
                fontColor={fontColor}
                style={style}
                value={valueInput}
                placeholder={{label: 'nível de ensino', value: null}}
                onValueChange={value => {
                    input.onChange(value)
                    setValueInput(value)
                }}
                items={options}
            />
        </ColNB>
    )
}

export default SelectLevelEducation;
