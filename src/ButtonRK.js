import PropTypes from 'prop-types'
import React from "react";
import {Button} from "native-base";
import TextRK from "./TextRK";
import {DIMENSION} from "../../../src/rkConfig/commonVariables";
import {getColor} from "./utils/commonStyle";

const ButtonRK = props => {
    const {rounded, fontSize, fontColor, style, fontBold, color, full, onPress, outline} = props

    const defaultStyleButton = {
        backgroundColor: outline ? 'transparent' : getColor(color),
        borderWidth: 1,
        borderColor: getColor(color),
        width: full ? '100%' : 'auto',
        ...style
    }

    return (
        <Button rounded={rounded} style={defaultStyleButton} onPress={onPress}>
            <TextRK size={fontSize || DIMENSION.buttonFontSize} color={fontColor} bold={fontBold} style={{textAlign: 'center', width: '100%'}}>{props.text}</TextRK>
        </Button>
    )
}

export default ButtonRK

ButtonRK.propTypes = {
    color: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    fontBold: PropTypes.bool,
    fontColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    fontSize: PropTypes.number,
    rounded: PropTypes.bool,
    style: PropTypes.object,
    text: PropTypes.string
}
