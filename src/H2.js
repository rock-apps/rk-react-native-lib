import PropTypes from 'prop-types'
import React from "react"
import TextRK from "./TextRK";
import {DIMENSION} from "../../../src/rkConfig/commonVariables";


const H2 = props => {

    const {size, color, style, numberOfLines, fontFamily, bold, uppercase, text, children} = props;

    return <TextRK size={DIMENSION.H2Size} color={color} style={style} numberOfLines={numberOfLines} fontFamily={fontFamily}
                   bold={bold} uppercase={uppercase} text={text}>{children}</TextRK>
}

export default H2

H2.propTypes = {
    bold: PropTypes.any,
    children: PropTypes.any,
    color: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    fontFamily: PropTypes.oneOf(['default', 'primary']),
    numberOfLines: PropTypes.number,
    size: PropTypes.number,
    style: PropTypes.object,
    text: PropTypes.string,
    uppercase: PropTypes.bool
}
