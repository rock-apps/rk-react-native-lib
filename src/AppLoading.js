import PropTypes from 'prop-types'
import React from "react"
import {Dimensions, View} from "react-native"
import ColNB from "./ColNB";
import {COLOR, DIMENSION, LOADING} from "../../../src/rkConfig/commonVariables";
import LottieView from 'lottie-react-native';


const {width, height} = Dimensions.get('window');

const AppLoading = props => {

    const {isLoading, lottie, widthLottie, heightLottie} = props;

    if(!isLoading) return null;

    return (
        <ColNB
            style={{
                position: 'absolute',
                width: width,
                height: height,
                backgroundColor: COLOR.backdrop,
                justifyContent: 'center',
                alignItems: 'center',
                zIndex: 9999
            }}
        >
            {lottie && (
                <LottieView
                    autoPlay
                    style={{
                        width: widthLottie || DIMENSION.widthLottie,
                        height: heightLottie || DIMENSION.heightLottie,
                    }}
                    source={LOADING}
                    // OR find more Lottie files @ https://lottiefiles.com/featured
                    // Just click the one you like, place that file in the 'assets' folder to the left, and replace the above 'require' statement
                />
            ) || (
                <View>
                    {props.children}
                </View>
            )}

        </ColNB>
    )
}

export default AppLoading;

AppLoading.propTypes = {
    children: PropTypes.any,
    heightLottie: PropTypes.any,
    isLoading: PropTypes.bool,
    lottie: PropTypes.any,
    widthLottie: PropTypes.any
}
