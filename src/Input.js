import PropTypes from 'prop-types'
import React from "react";
import {TextInput} from "react-native";
import {FONT, COLOR, DIMENSION} from "../../../src/rkConfig/commonVariables";
import {getColor} from "./utils/commonStyle";


const Input = props => {

    const {colorLabel, textAlign, borderColor, style, placeholderTextColor, focused, setFocused} = props

    let textInput = ''
    let styles = [
        {color: getColor(colorLabel), fontFamily: FONT.font_input, textAlign: textAlign || 'left', height: DIMENSION.inputHeight},
        focused && { borderColor: getColor(borderColor)},
        style
    ]

    const value = props.value || props.input.value


    return (
            <TextInput
                onTouchStart={props.onTouchStart}
                onFocus={() => {
                    props.input.onFocus && props.input.onFocus()
                    setFocused(true)
                }}
                onBlur={() => {
                    props.input.onBlur && props.input.onBlur()
                    setFocused(false)
                }}
                ref={c => (textInput = c)}
                placeholderTextColor={getColor(placeholderTextColor)}
                style={styles}
                placeholder={props.placeholder ? props.placeholder : ''}
                value={value}
                onChange={props.input.onChange}
                editable={!props.disabled}
                autoCapitalize={props.autoCapitalize}
            />
    )
}

export default Input;

Input.propTypes = {
  autoCapitalize: PropTypes.bool,
  borderColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
  colorLabel: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
  disabled: PropTypes.bool,
  input: PropTypes.any,
  onTouchStart: PropTypes.func,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
  style: PropTypes.object,
  textAlign: PropTypes.oneOf(['left','center','right']),
  value: PropTypes.any,
  focused: PropTypes.bool,
  setFocused: PropTypes.func
}
