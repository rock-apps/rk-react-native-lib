import React from "react"
import ColNB from "./ColNB";
import RowNB from "./RowNB";
import {FONT, COLOR, DRAWER, DIMENSION, ICONS} from "../../../src/rkConfig/commonVariables";
import {TouchableOpacity} from "react-native";


const Header = props => {

    const {left, body, right, back, menu, navigation} = props

    return (
        <RowNB style={{height: DIMENSION.heightHeader, backgroundColor: COLOR.header, flex: 0}}>
            <ColNB style={{flex: 0, justifyContent: 'center', paddingHorizontal: 10, paddingTop: 20}}>
                {back && (
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                    >
                        {ICONS.back}
                    </TouchableOpacity>
                )}
                {menu && (
                    <TouchableOpacity
                        onPress={() => navigation.toggleDrawer()}
                    >
                        {ICONS.menu}
                    </TouchableOpacity>
                )}
            </ColNB>
            <ColNB style={{flex: 1, justifyContent: 'center', paddingHorizontal: 5, paddingTop: 20, alignItems: 'center'}}>
                {body}
            </ColNB>
            <ColNB style={{flex: 0, justifyContent: 'center', paddingHorizontal: 5, paddingTop: 20, minWidth: 45}}>
                {right}
            </ColNB>

        </RowNB>
    )
}

export default Header;
