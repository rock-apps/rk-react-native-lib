import React from 'react';
import InputMask from "./InputMask";
import {getColor} from "./utils/commonStyle";

const InputCardNumber = props => {

    const {input, placeholder, style, focused, setFocused, textAlign} = props;

    let textInput = '';


    return (
            <InputMask
                input={input}
                setFocused={(value) => setFocused(value)}
                focused={focused}
                ref={c => (textInput = c)}
                type={'credit-card'}
                placeholderTextColor={getColor(props.placeholderTextColor)}
                onChangeText={text => input.onChange(text)}
                placeholder={placeholder || '0000.0000.0000.0000'}
                style={style}
                keyboardType='phone-pad'
                textAlign={textAlign}
            />
    );
}

export default InputCardNumber
