import PropTypes from 'prop-types'
import React from "react";
import RowNB from "./RowNB";
import {View} from "react-native"
import {getColor} from "./utils/commonStyle";

const ProgressBar = props => {

    const {height, percentage, style, bgColor, borderColor, progressColor, progressStyle} = props

    const styles = {
        width: '100%',
        height: height,
        borderColor: getColor(borderColor),
        backgroundColor: getColor(bgColor),
        borderWidth: 0.5,
        borderRadius: 2,
        ...style
    }
    const progressStyles = {
        width: `${percentage * 100}%`,
        height: '100%',
        backgroundColor: getColor(progressColor),
        ...progressStyle
    }

    return (
        <RowNB style={{height: height}}>
            <View style={styles}>
                <View style={progressStyles}/>
            </View>
        </RowNB>
    )
}

export default ProgressBar

ProgressBar.propTypes = {
    bgColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    borderColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    progressColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    height: PropTypes.number,
    percentage: PropTypes.number,
    style: PropTypes.object
}
