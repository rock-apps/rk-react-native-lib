'use strict';

import React, {Component} from 'react';
import {TEST} from "../../../src/rkConfig/commonVariables"
import {View, TouchableOpacity, Text} from 'react-native';
import computeProps from './utils/computeProps';
import computeStyleProps from './utils/computeStyleProps';
import defaultPropTypes from './utils/defaultPropTypes';

const RowNB = props => {

    let _root = ''
    const prepareRootProps = () => {

        let style = computeStyleProps(props);

        style.flexDirection = 'row';
        style.flex = (props.size) ? props.size : (props.style && props.style.height) ? 0 : 1;

        return computeProps(props, {style: style});
    }

    const setNativeProps = (nativeProps) => {
        _root.setNativeProps(nativeProps);
    }

    if (props.onPress) {
        return (
            <TouchableOpacity onPress={props.onPress}
                              {...prepareRootProps()}>
                <View
                    ref={component => _root = component}
                    {...props}
                    {...prepareRootProps()}
                >
                    {props.children}
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <View
            ref={component => _root = component}
            {...props}
            {...prepareRootProps()}
        >
            {props.children}
        </View>
    );
}

export default RowNB;

RowNB.propTypes = defaultPropTypes;
