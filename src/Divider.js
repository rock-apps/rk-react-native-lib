import React from "react"
import {View} from "react-native";
import {getColor} from "./utils/commonStyle";
import {FONT, COLOR, DIMENSION} from "../../../src/rkConfig/commonVariables";


const Divider = props => {
    const {style, color} = props;

    const styles = {
        borderBottomWidth: DIMENSION.dividerHeight,
        borderBottomColor: getColor(color),
        width: DIMENSION.dividerWidth,
        ...style
    }

    return (<View style={styles}/>)
}

export default Divider
