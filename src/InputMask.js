import PropTypes from 'prop-types'
import React from 'react';
import {FONT, COLOR, DIMENSION} from "../../../src/rkConfig/commonVariables";
import {TextInputMask} from "react-native-masked-text";
import {getColor} from "./utils/commonStyle";

const InputMask = props => {

    const {input, inverted, placeholder, placeholderTextColor, focused, setFocused, colorLabel, borderColor, type, keyboardType, style, textAlign} = props;


    let textInput = '';

    let styles = [
        {color: getColor(colorLabel), textAlign: textAlign || 'left', height: DIMENSION.inputHeight},
        focused && {
            borderColor: getColor(borderColor),
            shadowColor: "#BBFFEF",
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.43,
            shadowRadius: 9.51,

            elevation: 15,
        },
        style
    ]

    return (

        <TextInputMask
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            ref={c => (textInput = c)}
            type={type}
            value={input.value}
            placeholderTextColor={getColor(placeholderTextColor)}
            onChangeText={text => input.onChange(text)}
            placeholder={placeholder}
            style={styles}
            keyboardType={keyboardType}
        />
    );
}

export default InputMask

InputMask.propTypes = {
    borderColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    colorLabel: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    focused: PropTypes.bool,
    input: PropTypes.any,
    inverted: PropTypes.any,
    keyboardType: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    setFocused: PropTypes.func,
    type: PropTypes.oneOf(['phone-pad']),
    textAlign: PropTypes.oneOf(['left', 'right', 'center'])
}
