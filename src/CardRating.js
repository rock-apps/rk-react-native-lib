import PropTypes from 'prop-types'
import React from "react"
import RowNB from "./RowNB";
import ColNB from "./ColNB";
import CardRK from "./CardRK"
import TextRK from "./TextRK";
import InputRating from "./InputRating";
import Avatar from "./Avatar";

const CardRating = props => {

    const {name,date,comment, styleCard,styleContentComment, fontSizeName, fontColorName, fontSizeDate, fontColorDate, fontSizeComment, fontColorComment} = props
    const {starSize, rating, footer, avatar} = props

    const defaultStyleCard = {
        padding: 20,
        zIndex: 1,
        ...styleCard
    }

    const defaultStyleContentComment = {
        marginTop: 10,
        ...styleContentComment
    }

    return (
        <ColNB>
            <ColNB style={{position: 'absolute', zIndex: 2, top: -10, left: -10}}>
                <Avatar size={10} avatar={avatar} />
            </ColNB>
            <CardRK style={defaultStyleCard}>
                <RowNB><TextRK size={fontSizeName} color={fontColorName}>{name}</TextRK></RowNB>
                <RowNB>
                    <ColNB pullLeft><InputRating showRating={false} count={5} defaultRating={rating} size={starSize} starStyle={{marginHorizontal: 2}}/></ColNB>
                    {date && (<ColNB pullRight><TextRK size={fontSizeDate} color={fontColorDate}>{date}</TextRK></ColNB>)}
                </RowNB>
                <RowNB style={defaultStyleContentComment}>
                    <TextRK size={fontSizeComment} color={fontColorComment} style={{textAlign: 'justify'}}>
                        {comment}
                    </TextRK>
                </RowNB>
                <RowNB>{footer}</RowNB>
            </CardRK>
        </ColNB>
    )
}

export default CardRating;

CardRating.propTypes = {
    avatar: PropTypes.any,
    comment: PropTypes.any.isRequired,
    date: PropTypes.any,
    fontColorComment: PropTypes.string,
    fontColorDate: PropTypes.string,
    fontColorName: PropTypes.string,
    fontSizeComment: PropTypes.number,
    fontSizeDate: PropTypes.number,
    fontSizeName: PropTypes.number,
    footer: PropTypes.element,
    name: PropTypes.string.isRequired,
    rating: PropTypes.number,
    starSize: PropTypes.number,
    styleCard: PropTypes.object,
    styleContentComment: PropTypes.object
}
