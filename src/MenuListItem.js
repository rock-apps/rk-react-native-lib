import React from "react"
import {TouchableOpacity} from "react-native"
import RowNB from "./RowNB";
import Divider from "./Divider";
import ColNB from "./ColNB";
import {getColor} from "./utils/commonStyle";

const MenuListItem = props => {

    const {onPress, left, body, right, height, divider, colorDivider, full, style} = props;

    return (
        <TouchableOpacity
            onPress={() => onPress()}
            style={{flex: 1}}
        >
            <ColNB style={{flex: 0}}>

                <RowNB style={[style,{flex: 0}]}>
                    <ColNB style={{justifyContent: 'center', flex: 0}}>
                        {left}
                    </ColNB>
                    <ColNB style={{marginLeft: left && 15 || 0, justifyContent: 'center'}}>
                        {body}
                    </ColNB>
                    <ColNB style={{justifyContent: 'center', flex: 0}}>
                        {right}
                    </ColNB>
                </RowNB>
            </ColNB>
            <ColNB style={{}}>

                <Divider color={colorDivider} style={{marginTop: 5}}/>
            </ColNB>
        </TouchableOpacity>
    )
}

export default MenuListItem;
