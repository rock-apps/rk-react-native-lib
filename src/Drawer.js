import React from "react";
import {Container, Content} from "native-base"
import {FONT, COLOR, DRAWER} from "../../../src/rkConfig/commonVariables";


const Menu = props => {

    const {children} = props;

    return (
        <Container>
            <Content>
                {children}
            </Content>
        </Container>
    )
};

const Drawer = {
    contentComponent: props => <Menu {...props} />,
    ...DRAWER,
};

export default Drawer;
