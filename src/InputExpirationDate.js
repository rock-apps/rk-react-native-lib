import React from 'react';
import InputMask from "./InputMask";

const InputExpirationDate = props => {

    const {input, inverted, placeholder, style, focused, setFocused, textAlign} = props;

    let textInput = '';

    return (
            <InputMask
                input={input}
                setFocused={(value) => setFocused(value)}
                focused={focused}
                ref={c => (textInput = c)}
                type={'datetime'}
                options={{
                    format: 'MM/YY'
                }}
                value={input.value}
                placeholderTextColor={props.placeholderTextColor}
                onChangeText={text => input.onChange(text)}
                placeholder={placeholder}
                style={style}
                keyboardType='phone-pad'
                textAlign={textAlign}
            />
    );
}

export default InputExpirationDate
