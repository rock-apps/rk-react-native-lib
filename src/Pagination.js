import PropTypes from 'prop-types'
import React from "react";

import {Pagination} from "react-native-snap-carousel";

const PaginationRK = props => {

    const {length, activeSlide, containerStyle, dotStyle, inactiveDotOpacity, inactiveDotScale, dotContainerStyle} = props

    return (
        <Pagination
            dotsLength={length}
            activeDotIndex={activeSlide}
            containerStyle={[{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }, containerStyle]}
            dotContainerStyle={dotContainerStyle}
            dotStyle={[{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: 'rgba(255, 255, 255, 0.92)'
            }, dotStyle]}
            inactiveDotStyle={{
                // Define styles for inactive dots here
            }}
            inactiveDotOpacity={inactiveDotOpacity || 0.4}
            inactiveDotScale={inactiveDotScale || 0.6}

        />
    )
}

export default PaginationRK

PaginationRK.propTypes = {
    activeSlide: PropTypes.number.isRequired,
    containerStyle: PropTypes.object,
    dotStyle: PropTypes.object,
    inactiveDotOpacity: PropTypes.number,
    inactiveDotScale: PropTypes.number,
    length: PropTypes.number.isRequired
}
