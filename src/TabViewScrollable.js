import PropTypes from 'prop-types'
import React, {useState} from "react"
import {SceneMap, TabBar, TabView} from "react-native-tab-view";
import {FONT, COLOR} from "../../../src/rkConfig/commonVariables";
import {Dimensions, Image, View} from "react-native";
import TextRK from "./TextRK";
import {getColor} from "./utils/commonStyle";

const TabViewScrollable = props => {
    const {iconMaster, routes, scene, withoutBorder, spaceLeft, indicatorStyle, activeColor, inactiveColor} = props

    const [state, setState] = useState({
        index: spaceLeft ? 1 : 0,
        routes: spaceLeft ? [{key: 'IconMaster', title: 'icon'}, ...routes] : [...routes]
    })

    let sceneMap = {
        ...scene
    }

    if (spaceLeft) {
        sceneMap = {
            IconMaster: () => null,
            ...scene
        }
    }

    return (
        <TabView
            navigation={props.navigation}
            navigationState={state}
            renderScene={SceneMap(sceneMap)}
            renderTabBar={props => <TabBar
                {...props}
                scrollEnabled={true}
                indicatorStyle={{backgroundColor: COLOR.liberi, zIndex: 2, height: 2, bottom: -1}}
                activeColor={getColor(activeColor)}
                inactiveColor={getColor(inactiveColor)}
                tabStyle={{alignItems: 'flex-start', flexWrap: 'nowrap', width: 'auto'}}
                tabsContainerStyle={{backgroundColor: COLOR.background}}
                renderLabel={({route, focused, color}) => {

                    if (route.key === 'IconMaster') {
                        return iconMaster &&
                            <Image source={iconMaster} style={{width: 32, height: 32, marginLeft: 5}}/> ||
                            <View style={{width: 10, marginLeft: 5}}/>;
                    }
                    return (
                        <TextRK
                            color={focused ? activeColor : inactiveColor}
                            size={18}
                            style={{marginBottom: -15, zIndex: 9999}}
                        >
                            {route.title}
                        </TextRK>
                    )
                }}
                style={{
                    flex: 0,
                    backgroundColor: 'transparent',
                    width: '100%',
                    flexWrap: 'nowrap',
                    borderBottomWidth: withoutBorder ? 0 : 1,
                    borderBottomColor: '#00FFFF',
                    zIndex: 999
                }}
            />
            }
            onIndexChange={index => {
                if (spaceLeft) {
                    if (index > 0) {
                        setState({...state, index: index})
                    }
                } else {
                    setState({...state, index: index})
                }
            }}
            initialLayout={{width: Dimensions.get('window').width}}
        />
    )
}

export default TabViewScrollable;

TabViewScrollable.propTypes = {
    activeColor: PropTypes.any,
    iconMaster: PropTypes.any,
    inactiveColor: PropTypes.any,
    indicatorStyle: PropTypes.any,
    navigation: PropTypes.any,
    routes: PropTypes.any,
    scene: PropTypes.any,
    spaceLeft: PropTypes.boll,
    withoutBorder: PropTypes.any
}
