import PropTypes from 'prop-types'
import React from "react";
import {ImageBackground} from "react-native"
import RowNB from "./RowNB";
import ColNB from "./ColNB";

const CardImageBG = props => {

    const {bg, elementTopLeft, styleContainerTopLeft, elementTopRight, styleContainerTopRight, elementTopBar, styleContainerTopBar} = props;
    const {elementBottomLeft, styleContainerBottomLeft, elementBottomRight, styleContainerBottomRight, elementBottomBar, styleContainerBottomBar} = props;
    const {cardRadius, minHeight} = props;

    return (
        <ColNB style={{minHeight: minHeight, borderRadius: cardRadius}}>
            <ImageBackground
                style={{width: '100%', height: '100%', justifyContent: 'space-between'}}
                imageStyle={{borderRadius: cardRadius}}
                source={bg}>
                <RowNB style={{justifyContent: 'space-between', padding: 10, position: 'absoulte', zIndex: 2}}>
                    <RowNB style={[{flex: 0}, styleContainerTopLeft]}>
                        {elementTopLeft}
                    </RowNB>
                    <RowNB style={[{flex: 0}, styleContainerTopRight]}>
                        {elementTopRight}
                    </RowNB>
                </RowNB>
                {elementTopBar && (
                    <ColNB style={[{
                        flex: 0,
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,.5)',
                        width: '100%',
                        height: 'auto',
                        padding: 10,
                        borderTopLeftRadius: cardRadius,
                        borderTopRightRadius: cardRadius,
                        zIndex: 1,
                    }, styleContainerTopBar]}>
                        {elementTopBar}
                    </ColNB>
                )}

                {/*Parte do bottom*/}
                <RowNB style={{
                    justifyContent: 'space-between',
                    padding: 10,
                    position: 'absoulte',
                    zIndex: 2,
                    bottom: 0,
                    alignItems: 'flex-end'
                }}>
                    <RowNB style={[{flex: 0}, styleContainerBottomLeft]}>
                        {elementBottomLeft}
                    </RowNB>
                    <RowNB style={[{flex: 0}, styleContainerBottomRight]}>
                        {elementBottomRight}
                    </RowNB>
                </RowNB>
                {elementBottomBar && (
                    <ColNB style={[{
                        flex: 0,
                        position: 'absolute',
                        backgroundColor: 'rgba(0,0,0,.5)',
                        width: '100%',
                        height: 'auto',
                        padding: 10,
                        bottom: 0,
                        borderBottomLeftRadius: cardRadius,
                        borderBottomRightRadius: cardRadius,
                        zIndex: 1,
                    }, styleContainerBottomBar]}>
                        {elementBottomBar}
                    </ColNB>
                )}
            </ImageBackground>
        </ColNB>
    )
}

export default CardImageBG;

CardImageBG.propTypes = {
    bg: PropTypes.shape,
    cardRadius: PropTypes.number,
    elementBottomBar: PropTypes.element,
    elementBottomLeft: PropTypes.element,
    elementBottomRight: PropTypes.element,
    elementTopBar: PropTypes.element,
    elementTopLeft: PropTypes.element,
    elementTopRight: PropTypes.element,
    minHeight: PropTypes.number,
    styleContainerBottomBar: PropTypes.object,
    styleContainerBottomLeft: PropTypes.object,
    styleContainerBottomRight: PropTypes.object,
    styleContainerTopBar: PropTypes.object,
    styleContainerTopLeft: PropTypes.object,
    styleContainerTopRight: PropTypes.object
}
