import PropTypes from 'prop-types'
import React from "react";
import ColNB from "./ColNB";
import RowNB from "./RowNB";
import InputRating from "./InputRating";
import ProgressBar from "./ProgressBar"
import TextRK from "./TextRK";

const RatingWithBar = props => {

    const {data, starSize, progressBgColor, progressColor, progressHeight, fontSize, fontColor, fontBold} = props

    return (
        <ColNB style={{width: 'auto'}}>
            <RowNB style={{flex: 0}}>
                <ColNB style={{flex: 0}}>

                    {data.length > 0 && data.map(item => (
                        <ColNB style={{alignItems: 'flex-end', marginRight: 5}}>
                            <InputRating size={starSize || 10} count={item.star} showRating={false}
                                         starStyle={{marginHorizontal: 0}}
                                         defaultRating={item.star}/>
                        </ColNB>
                    ))}
                </ColNB>
                <ColNB style={{flex: 1}}>
                    {data.length > 0 && data.map(item => (
                        <RowNB>
                            <ColNB style={{justifyContent: 'center'}}>
                                <ProgressBar
                                    height={progressHeight || 7}
                                    bgColor={progressBgColor || 'transparent'}
                                    percentage={item.progress}
                                    progressColor={progressColor || 'error'}
                                    borderColor={'transparent'}
                                    progressStyle={{borderRadius: 8}}/>
                            </ColNB>
                            <ColNB style={{flex: 0, justifyContent: 'center', alignItems: 'center'}}>
                                <TextRK size={fontSize} bold={fontBold} color={fontColor}>{item.qtd}</TextRK>
                            </ColNB>
                        </RowNB>
                    ))}

                </ColNB>
            </RowNB>
        </ColNB>
    )
}

export default RatingWithBar;

RatingWithBar.propTypes = {
    data: PropTypes.array,
    fontBold: PropTypes.bool,
    fontColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background', 'black', 'gray', 'disabled']),
    fontSize: PropTypes.number,
    progressBgColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background', 'black', 'gray', 'disabled']),
    progressColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background', 'black', 'gray', 'disabled']),
    progressHeight: PropTypes.number,
    starSize: PropTypes.number
}
