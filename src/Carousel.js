import PropTypes from 'prop-types'
import React, {useState} from "react"
import Carousel from "react-native-snap-carousel";

import ColNB from "./ColNB";
import PaginationRK from "./Pagination";

const CarouselRK = props => {

    const {data, renderItem, sliderWidth, itemWidth, enableSnap, pagination, paginationDotStyle, paginationContainerStyle, inactiveDotOpacity, inactiveDotScale} = props;
    const [activeSlide, setActiveSlide] = useState(0)

    return (
        <ColNB style={{marginLeft: -20}}>
            {data.length > 0 && (
                <>
                    <Carousel
                        data={data}
                        renderItem={renderItem}
                        sliderWidth={sliderWidth}
                        itemWidth={itemWidth}
                        enableSnap={enableSnap}
                        onSnapToItem={(index) => setActiveSlide(index)}
                    />
                    {pagination && (
                        <ColNB style={{width: sliderWidth, flex: 0}}>
                            <PaginationRK
                                length={data.length} activeSlide={activeSlide} dotStyle={paginationDotStyle}
                                containerStyle={paginationContainerStyle} inactiveDotOpacity={inactiveDotOpacity}
                                inactiveDotScale={inactiveDotScale}
                            />
                        </ColNB>
                    )}
                </>
            )}
        </ColNB>
    )
}

export default CarouselRK

CarouselRK.propTypes = {
    data: PropTypes.array.isRequired,
    enableSnap: PropTypes.bool,
    inactiveDotOpacity: PropTypes.number,
    inactiveDotScale: PropTypes.number,
    itemWidth: PropTypes.number,
    pagination: PropTypes.bool,
    paginationContainerStyle: PropTypes.object,
    paginationDotStyle: PropTypes.object,
    renderItem: PropTypes.func,
    sliderWidth: PropTypes.number
}
