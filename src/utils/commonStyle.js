import {COLOR} from "../../../../src/rkConfig/commonVariables"

export const getColor = (data) => {
    switch (data) {
        case 'alert':
            return COLOR.alert;
        case 'secondary':
            return COLOR.secondary;
        case 'light':
            return COLOR.light;
        case 'light_blur':
            return COLOR.light_blur;
        case 'warning':
            return COLOR.warning;
        case 'error':
            return COLOR.error;
        case 'primary':
            return COLOR.primary;
        case 'success':
            return COLOR.success;
        case 'liberi':
            return COLOR.liberi;
        case 'background':
            return COLOR.background;
        case 'black':
            return COLOR.black;
        case 'gray':
            return COLOR.gray;
        case 'disabled':
            return COLOR.disabled;
        case '' || undefined:
            return COLOR.default;
        default:
            return data;
    }
}
