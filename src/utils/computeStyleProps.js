import React from "react";
import {StyleSheet} from "react-native";
import _ from 'lodash';

function computeStyleProps({style, pullLeft, pullRight, center, children, height, width, ph, pv, pl, pr, pt, pb, mh, mv, ml, mr, mt, mb, ...props}) {

    const newStyle = {};

    const defaultSpace = 2;

    /**
     * Testa para os valores p0,p1,... ph0,ph1...
     */

    let i;
    for (i = 0; i <= 10; i++) {
        if (props['p' + i]) newStyle.padding = i * defaultSpace;
        if (props['ph' + i]) newStyle.paddingHorizontal = i * defaultSpace;
        if (props['pv' + i]) newStyle.paddingVertical = i * defaultSpace;
        if (props['pl' + i]) newStyle.paddingLeft = i * defaultSpace;
        if (props['pr' + i]) newStyle.paddingRight = i * defaultSpace;
        if (props['pt' + i]) newStyle.paddingTop = i * defaultSpace;
        if (props['pb' + i]) newStyle.paddingBottom = i * defaultSpace;
        if (props['m' + i]) newStyle.margin = i * defaultSpace;
        if (props['mh' + i]) newStyle.marginHorizontal = i * defaultSpace;
        if (props['mv' + i]) newStyle.marginVertical = i * defaultSpace;
        if (props['ml' + i]) newStyle.marginLeft = i * defaultSpace;
        if (props['mr' + i]) newStyle.marginRight = i * defaultSpace;
        if (props['mt' + i]) newStyle.marginTop = i * defaultSpace;
        if (props['mb' + i]) newStyle.marginBottom = i * defaultSpace;
    }

    if (ph && ph.isNumber()) newStyle.paddingHorizontal = ph;
    if (pv && pv.isNumber()) newStyle.paddingVertical = pv;
    if (pl && pl.isNumber()) newStyle.paddingLeft = pl;
    if (pr && pr.isNumber()) newStyle.paddingRight = pr;
    if (pt && pt.isNumber()) newStyle.paddingTop = pt;
    if (pb && pb.isNumber()) newStyle.paddingBottom = pb;

    if (mh && mh.isNumber()) newStyle.marginHorizontal = mh;
    if (mv && mv.isNumber()) newStyle.marginVertical = mv;
    if (ml && ml.isNumber()) newStyle.marginLeft = ml;
    if (mr && mr.isNumber()) newStyle.marginRight = mr;
    if (mt && mt.isNumber()) newStyle.marginTop = mt;
    if (mb && mb.isNumber()) newStyle.marginBottom = mb;

    if (height) newStyle.height = height;
    if (width) newStyle.width = width;
    if (pullLeft) newStyle.alignItems = 'flex-start';
    if (pullRight) newStyle.alignItems = 'flex-end';
    if (center) {
        newStyle.alignSelf = 'center';
        newStyle.justifyContent = 'center';
        newStyle.alignItems = 'center';
    }

    return newStyle;
}

export default computeStyleProps;
