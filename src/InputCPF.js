import React, {useState} from 'react';
import InputMask from "./InputMask";

const InputCPF = props => {

    const {input, inverted, placeholder, style, focused, setFocused, textAlign} = props;

    let textInput = '';

    return (
            <InputMask
                input={input}
                setFocused={(value) => setFocused(value)}
                focused={focused}
                ref={c => (textInput = c)}
                type={'cpf'}
                placeholderTextColor={props.placeholderTextColor}
                onChangeText={text => input.onChange(text)}
                placeholder={placeholder || 'CPF'}
                style={style}
                keyboardType='phone-pad'
                textAlign={textAlign}
            />
    );
}

export default InputCPF
