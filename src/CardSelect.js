import React, {useEffect, useState} from "react"
import ColNB from "./ColNB";
import TextRK from "./TextRK"
import {Content} from "native-base";
import {Dimensions, Animated} from "react-native";
import ButtonRK from "./ButtonRK";
import {PanGestureHandler, State} from "react-native-gesture-handler";
import {COLOR} from "../../../src/rkConfig/commonVariables";

const {height} = Dimensions.get('screen');

const CardSelect = props => {

    if (!props.isOpen) return null;
    const {isOpen, onCancel} = props

    let offSet = 0

    const [translateY] = useState(new Animated.Value(500))

    const animatedEvent = Animated.event(
        [
            {
                nativeEvent: {
                    translationY: translateY,
                }
            }
        ],
        {
            useNativeDriver: true
        }
    )


    useEffect(() => {
        Animated.timing(
            translateY,
            {
                toValue: 0,
                duration: 1000,
                useNativeDriver: true
            },
        ).start();
    }, [])



    const onHandlerStateChanged = (event) => {
        let opened = false
        if(event.nativeEvent.oldState === State.ACTIVE) {
            const {translationY} = event.nativeEvent;
            offSet += translationY

            if(translationY >= 150) {
                opened = true
            } else {

            }

            Animated.timing(translateY, {
                toValue: opened ? 500 : 0,
                duration: 800,
                useNativeDriver: true
            }).start(() => {opened && onCancel()});
        }
    }

    return (

        <Animated.View style={{
            height: height,
            width: '100%',
            backgroundColor: COLOR.backdrop,
            position: 'absolute',
            top: 0,
            opacity: translateY.interpolate({
                inputRange: [0,500],
                outputRange: [1,0],
                extrapolate: 'clamp'
            }),
            zIndex: 999
        }}>
            <PanGestureHandler
                onGestureEvent={animatedEvent}
                onHandlerStateChange={onHandlerStateChanged}
            >
                <Animated.View style={[{
                    height: height * 0.70,
                    backgroundColor: 'white',
                    position: 'absolute',
                    bottom: 0,
                    width: '100%',
                    borderTopRightRadius: 40,
                    borderTopLeftRadius: 40,
                    paddingTop: 40,
                    transform: [{
                        translateY: translateY.interpolate({
                            inputRange: [-200, 0,500],
                            outputRange: [-50, 0, 500],
                            extrapolate: 'clamp'
                        })
                    }]
                }]}>
                    <ColNB center style={{flex: 0, marginTop: -20, marginBottom: 0, backgroundColor: 'gray', height: 5, width: 100, borderRadius: 10}}/>
                    <Content>
                        {props.children}
                    </Content>
                </Animated.View>
            </PanGestureHandler>

        </Animated.View>
    )
}

export default CardSelect
