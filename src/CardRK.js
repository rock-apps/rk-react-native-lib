import PropTypes from 'prop-types'
import React from "react";
import {Card} from 'native-base'


const CardRK = props => {

    const {style} = props

    return <Card style={style}>{props.children}</Card>
}

export default CardRK

CardRK.propTypes = {
    children: PropTypes.any,
    style: PropTypes.object
}
