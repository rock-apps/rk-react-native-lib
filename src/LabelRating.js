import React from "react";
import RowNB from "./RowNB";
import TextRK from "./TextRK";
import {getColor} from "./utils/commonStyle";

const LabelRating = props => {

    const {fontColor, fontSize, bgColor, bold, rating, icon, radius} = props;

    return (
        <RowNB style={{paddingHorizontal: 5, borderRadius: radius, backgroundColor: getColor(bgColor), flex: 0, justifyContent: 'center', alignItems: 'center'}}>
            <TextRK size={fontSize} bold color={fontColor}>{rating}</TextRK>
            {icon}
        </RowNB>
    )
}

export default LabelRating;
