import PropTypes from 'prop-types'
import React from "react";
import {AirbnbRating} from "react-native-ratings";


const InputRating = props => {

    const {reviews, size, showRating, defaultRating, count, disabled, starContainerStyle, reviewSize, input, onFinishRating, style, starStyle} = props;

    const defaulStarStyle = {
        marginHorizontal: 5,
        ...starStyle
    }

    return (
        <AirbnbRating
            reviews={reviews}
            size={size}
            showRating={showRating}
            defaultRating={defaultRating}
            count={count || 5}
            isDisabled={disabled}
            starContainerStyle={starContainerStyle}
            reviewSize={reviewSize}
            starStyle={defaulStarStyle}

            onFinishRating={(value) =>{
                if(input) {
                    input.change(value)
                }
                if(onFinishRating) {
                    onFinishRating(value)
                }
            }}
            style={style}
        />
    )
}

export default InputRating;

InputRating.propTypes = {
    count: PropTypes.number,
    defaultRating: PropTypes.number,
    disabled: PropTypes.bool,
    input: PropTypes.any,
    onFinishRating: PropTypes.func,
    reviewSize: PropTypes.number,
    reviews: PropTypes.array,
    showRating: PropTypes.bool,
    size: PropTypes.number,
    starContainerStyle: PropTypes.object,
    starStyle: PropTypes.string,
    style: PropTypes.string
}
