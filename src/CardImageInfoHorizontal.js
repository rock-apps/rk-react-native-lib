import PropTypes from 'prop-types'
import React from "react";
import ColNB from "./ColNB";
import CardRK from "./CardRK";
import {ImageBackground} from "react-native";

const CardImageInfoHorizontal = props => {

    const {height, source, styleElementCard, elementCard, sourceWidth, radius} = props
    const {elementTopRight, styleElementTopRight, elementBottomRight, styleElementBottomRight, elementBottomLeft, styleElementBottomLeft, elementTopLeft, styleElementTopLeft} = props
    const styleElementCardDefault = {
        flex: 1,
        width: '100%',
        padding: 10,
        ...styleElementCard
    }

    return (
        <CardRK style={{height: height || 150, flexDirection: 'row', borderRadius: radius}}>
            <ColNB style={[{flex: 0, position: 'absolute', right: 0}, styleElementTopRight]}>
                {elementTopRight}
            </ColNB>
            <ColNB style={[{flex: 0, position: 'absolute'}, styleElementTopLeft]}>
                {elementTopLeft}
            </ColNB>
            <ColNB style={{flex: 0}}>
                <ImageBackground style={{width: sourceWidth || 150, height: '100%'}} source={source} imageStyle={{borderTopLeftRadius: radius, borderBottomLeftRadius: radius}}>

                </ImageBackground>
            </ColNB>
            <ColNB style={styleElementCardDefault}>
                {elementCard}
            </ColNB>
            <ColNB style={[{flex: 0, position: 'absolute', bottom: 0, right: 0}, styleElementBottomRight]}>
                {elementBottomRight}
            </ColNB>
            <ColNB style={[{flex: 0, position: 'absolute', bottom: 0}, styleElementBottomLeft]}>
                {elementBottomLeft}
            </ColNB>
        </CardRK>
    );
}

export default CardImageInfoHorizontal;

CardImageInfoHorizontal.propTypes = {
    elementBottomLeft: PropTypes.element,
    elementBottomRight: PropTypes.element,
    elementCard: PropTypes.element,
    elementTopLeft: PropTypes.element,
    elementTopRight: PropTypes.element,
    height: PropTypes.number,
    radius: PropTypes.number,
    source: PropTypes.any,
    sourceWidth: PropTypes.number,
    styleElementBottomLeft: PropTypes.object,
    styleElementBottomRight: PropTypes.object,
    styleElementCard: PropTypes.object,
    styleElementTopLeft: PropTypes.object,
    styleElementTopRight: PropTypes.object
}
