import PropTypes from 'prop-types'
import React from "react"
import {getColor} from "./utils/commonStyle";
import {Image} from "react-native"

const Avatar = props => {
    const {avatar, borderColor, size, style} = props

    const styles = [
        borderColor && {borderColor: getColor(borderColor)},
        {
            width: 4 * size,
            height: 4 * size,
            borderWidth: borderColor && 1 || 0,
            borderRadius: (4 * size) / 2
        },
        style
    ]

    return (
        <Image
            style={styles}
            source={avatar}
        />
    )
}

export default Avatar

Avatar.propTypes = {
  avatar: PropTypes.any,
  borderColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
  size: PropTypes.number,
  style: PropTypes.object
}
