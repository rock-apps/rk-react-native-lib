'use strict';

import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import computeProps from './utils/computeProps';
import defaultPropTypes from "./utils/defaultPropTypes";
import computeStyleProps from "./utils/computeStyleProps";

const ColNB = props => {

    let _root = ''

    const prepareRootProps = () => {

        let style = computeStyleProps(props);

        style.flexDirection = 'column';
        style.flex = (props.size) ? props.size : (props.style && props.style.width) ? 0 : 1;

        return computeProps(props, {style: style});

    };

    const setNativeProps = (nativeProps) => {
        _root.setNativeProps(nativeProps);
    }

    if (props.onPress) {
        return (
            <TouchableOpacity onPress={props.onPress}
                              {...prepareRootProps()}>
                <View
                    ref={component => _root = component}
                    {...props}
                    {...prepareRootProps()}
                >{props.children}</View>
            </TouchableOpacity>
        );
    }

    return (
        <View
            ref={component => _root = component}
            {...props}
            {...prepareRootProps()}
        >
            {props.children}</View>

    );
}
ColNB.propTypes = defaultPropTypes;

export default ColNB






