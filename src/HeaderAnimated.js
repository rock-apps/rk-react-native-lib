import React, {useState} from "react"
import {Animated, Dimensions} from "react-native";
import {PanGestureHandler, State} from "react-native-gesture-handler";
import ColNB from "./ColNB";

const {height} = Dimensions.get('screen');

const HeaderAnimated = props => {

    const [translateY] = useState(new Animated.Value(0))

    let offSet = 0

    const animatedEvent = Animated.event(
        [
            {
                nativeEvent: {
                    translationY: translateY,
                }
            }
        ],
        {
            useNativeDriver: true
        }
    )


    const onHandlerStateChanged = (event) => {
        let opened = true

        if (event.nativeEvent.oldState === State.ACTIVE) {
            const {translationY} = event.nativeEvent;
            offSet += translationY
            if (translationY <= -50) {
                opened = false
            } else {
                translateY.setValue(offSet)
                translateY.setOffset(0)
                offSet = 0
            }

            Animated.timing(translateY, {
                toValue: opened ? 0 : -100,
                duration: 800,
                useNativeDriver: true
            }).start(() => {
                offSet = opened ? 0 : -100
                translateY.setOffset(offSet)
                translateY.setValue(0)
            });
        }
    }

    return (
        <ColNB style={{height: height, width: '100%'}}>
            <PanGestureHandler
                onGestureEvent={animatedEvent}
                onHandlerStateChange={onHandlerStateChanged}
            >
                <Animated.View style={{
                    height: 250,
                    width: '100%',
                    backgroundColor: 'white',
                    borderBottomLeftRadius: 40,
                    borderBottomRightRadius: 40,
                    transform: [{
                        translateY: translateY.interpolate({
                            inputRange: [-100, 0],
                            outputRange: [-100, 0],
                            extrapolate: 'clamp'
                        }),
                    }]
                }}>
                    <Animated.View style={{
                        position: 'absolute',
                        height: '100%',
                        width: '100%',
                        opacity: translateY.interpolate({
                            inputRange: [-50, 0],
                            outputRange: [0, 1]
                        })
                    }}>
                        {props.headerOpen}
                    </Animated.View>
                    <Animated.View style={{
                        position: 'absolute',
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        bottom: 0,
                        height: '50%',
                        width: '100%',
                        opacity: translateY.interpolate({
                            inputRange: [-100, -50],
                            outputRange: [1, 0]
                        })
                    }}>
                        {props.headerClose}
                    </Animated.View>

                </Animated.View>
            </PanGestureHandler>

        </ColNB>
    )
}

export default HeaderAnimated
