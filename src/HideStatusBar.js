import React from "react";
import {StatusBar} from "react-native";

const HideStatusBar = props => {
    return <StatusBar hidden={true} translucent={true}/>

}

export default HideStatusBar
