import PropTypes from 'prop-types'
import React from "react";
import ColNB from "./ColNB";
import Avatar from "./Avatar"
import {getColor} from "./utils/commonStyle";

const CardAvatarInfo = props => {

    const {avatar, borderAvatar, elementInfo, bgColor, radius} = props


    const cardStyleDefault = {
        justifyContent: 'center',
        alignItems: 'center',
    }

    const infoStyleDefault = {
        width: '100%', marginTop: 5, backgroundColor: getColor(bgColor),
        borderRadius: radius,
        padding: 10

    }

    return (
        <ColNB style={cardStyleDefault}>
            <Avatar avatar={avatar} size={30} borderColor={borderAvatar}/>
            <ColNB style={infoStyleDefault}>
                {elementInfo}
            </ColNB>
        </ColNB>
    )
}

export default CardAvatarInfo;

CardAvatarInfo.propTypes = {
    avatar: PropTypes.any,
    bgColor: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    borderAvatar: PropTypes.oneOf(['alert', 'secondary', 'light', 'light_blur', 'warning', 'error', 'primary', 'success', 'liberi', 'background','black','gray','disabled']),
    elementInfo: PropTypes.element,
    radius: PropTypes.number
}
