import PropTypes from 'prop-types'
import React, {useState} from "react";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
import {Keyboard, TouchableOpacity} from "react-native";
import Input from "./Input";

const InputDatePicker = props => {

    const [isDateTimePickerVisible, setPickerVisible] = useState(false);
    const {input, inverted, placeholder, mode, title, textAlign, allowFuture} = props;


    const renderValue = date => {
        const {mode} = props;

        if (mode === 'time') {
            const dateItems = date.split(':');
            return dateItems[0] + ':' + dateItems[1];
        }

        // Não pode testar a validade no timepicker pois dá erro - ele só traz o time e não a data
        const dateObj = moment(date);
        if (!dateObj.isValid()) {
            return null;
        }

        let format = 'DD/MM/YYYY';
        if (mode === 'date') {
            format = 'DD/MM/YYYY';
        }
        if (mode === 'datetime') {
            format = 'DD/MM/YYYY HH:mm';
        }

        return dateObj.format(format);
    };

    const handleDatePicked = date => {

        const {mode, input} = props;

        const dateObj = moment(date);
        if (!dateObj.isValid()) {
            return null;
        }
        if(!allowFuture && dateObj.diff(moment()) >= 0) {
            return null;
        }

        let value = null;
        switch (mode) {
            case 'date':
                value = dateObj.format('YYYY-MM-DD');
                break;
            case 'time':
                value = dateObj.format('HH:mm') + ':00';
                break;
            case 'datetime':
                value = dateObj.format('YYYY-MM-DD HH:mm') + ':00';
                break;
        }

        input.onChange(value);

        setPickerVisible(false)
    };

    const handleTouch = () => {
    }

    const value = renderValue(input.value);

    return (
        <>
            <DateTimePicker
                isVisible={isDateTimePickerVisible}
                onConfirm={handleDatePicked}
                onCancel={() => setPickerVisible(false)}
                titleIOS={title ? title : 'Selecione uma Data'}
                mode={mode}
            />
            <TouchableOpacity onPress={() => {}}>
                <Input
                    setFocused={() => {}}
                    onTouchStart={async () => {
                        await setTimeout(() => Keyboard.dismiss(),200)
                        await setPickerVisible(true)

                    }}
                    {...props}
                    disabled={false}
                    value={value}
                    textAlign={textAlign}
                />
            </TouchableOpacity>
        </>
    )
}

export default InputDatePicker

InputDatePicker.propTypes = {
    allowFuture: PropTypes.bool,
    input: PropTypes.any,
    inverted: PropTypes.bool,
    mode: PropTypes.oneOf(['date', 'time', 'datetime']),
    placeholder: PropTypes.string,
    textAlign: PropTypes.oneOf(['left', 'right', 'center']),
    title: PropTypes.string
}
